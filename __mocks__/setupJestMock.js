jest.mock('react-i18next', () => ({
    useTranslation: () => ({ t: (key) => key }),
}));

jest.mock('react-native/Libraries/Animated/NativeAnimatedHelper');

jest.mock('@react-navigation/native', () => ({
    useNavigation: () => ({ navigate: () => { } }),
}));

jest.mock('react-native/Libraries/Interaction/InteractionManager', () => ({
    runAfterInteractions: () => { },
    createInteractionHandle: () => ({
        start: () => { },
    }),
    clearInteractionHandle: () => ({
        start: () => { },
    }),
}));