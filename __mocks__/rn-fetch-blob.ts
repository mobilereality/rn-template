const existsMock = jest.fn();
existsMock.mockReturnValueOnce({ then: jest.fn() });

export default {
    DocumentDir: (): void => {},
    ImageCache: {
        get: {
            clear: (): void => {},
        },
    },
    fs: {
        exists: existsMock,
        dirs: {
            MainBundleDir: (): void => {},
            CacheDir: (): void => {},
            DocumentDir: (): void => {},
        },
    },
};
