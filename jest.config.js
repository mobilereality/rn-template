module.exports = {
    preset: 'react-native',
    globals: {
        'ts-jest': {
            babelConfig: true,
        },
    },
    transformIgnorePatterns: ['node_modules/(?!@react-native|react-native|@react-navigation/core|@react-navigation/native)'],
    testPathIgnorePatterns: ['/node_modules/'],
    testRegex: '/__tests__/.*\\.(test|spec)\\.(js|tsx?)$',
    transform: {
        '^.+\\.(ts|tsx)$': 'ts-jest',
    },
    moduleDirectories: ['node_modules', 'src'],
    moduleNameMapper: {
        '\\.svg': '<rootDir>/__mocks__/svgMock.js',
    },
    setupFiles: ['./node_modules/react-native-gesture-handler/jestSetup.js', './__mocks__/setupJestMock.js'],
};
