import React from 'react';
import App from './src/App';
import { enableScreens } from 'react-native-screens';
import { AppRegistry, LogBox, Text } from 'react-native';
import { name as appName } from './app.json';

enableScreens();

Text.defaultProps = Text.defaultProps || {};
Text.defaultProps.allowFontScaling = false;

LogBox.ignoreLogs(['VirtualizedLists should never be nested']);

function HeadlessCheck({ isHeadless }) {
    if (isHeadless) {
        // App has been launched in the background by iOS, ignore
        return null;
    }

    return <App />;
}

AppRegistry.registerComponent(appName, () => HeadlessCheck);
