/**
 * Metro configuration for React Native
 * https://github.com/facebook/react-native
 *
 * @format
 */

const path = require('path');
const { getDefaultConfig } = require('metro-config');

module.exports = (async () => {
    const {
        resolver: { sourceExts, assetExts },
    } = await getDefaultConfig();
    return {
        resolver: {
            assetExts: assetExts.filter(ext => ext !== 'svg'),
            sourceExts: [...sourceExts, 'svg'],
            extraNodeModules: {
                api: path.resolve(__dirname, 'src/api'),
                assets: path.resolve(__dirname, 'src/assets'),
                components: path.resolve(__dirname, 'src/components'),
                constants: path.resolve(__dirname, 'src/constants'),
                containers: path.resolve(__dirname, 'src/containers'),
                config: path.resolve(__dirname, 'src/config'),
                helpers: path.resolve(__dirname, 'src/helpers'),
                hocs: path.resolve(__dirname, 'src/hocs'),
                hooks: path.resolve(__dirname, 'src/hooks'),
                locales: path.resolve(__dirname, 'src/locales'),
                modules: path.resolve(__dirname, 'src/modules'),
                navigation: path.resolve(__dirname, 'src/navigation'),
                providers: path.resolve(__dirname, 'src/providers'),
                screens: path.resolve(__dirname, 'src/screens'),
                styles: path.resolve(__dirname, 'src/styles'),
                types: path.resolve(__dirname, 'src/types'),
                utils: path.resolve(__dirname, 'src/utils'),
            },
        },
        transformer: {
            babelTransformerPath: require.resolve('react-native-svg-transformer'),
            getTransformOptions: async () => ({
                transform: {
                    experimentalImportSupport: false,
                    inlineRequires: false,
                },
            }),
        },
    };
})();
