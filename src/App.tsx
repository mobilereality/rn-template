import React, { FC } from 'react';
import 'react-native-gesture-handler';
import { i18nInit } from 'locales/i18n';
import { SwrConfig } from 'modules/swr-config';
import { ErrorBoundary } from 'modules/error-boundary';
import { ConnectionStatusBoundary } from 'modules/connection-status-boundary';
import { AuthContextProvider } from 'providers/auth-context';
import { Authorization } from 'modules/authorization';

i18nInit();

const App: FC = () => {
    return (
        <ErrorBoundary>
            <ConnectionStatusBoundary>
                <AuthContextProvider>
                    <SwrConfig>
                        <Authorization />
                    </SwrConfig>
                </AuthContextProvider>
            </ConnectionStatusBoundary>
        </ErrorBoundary>
    );
};

export default App;
