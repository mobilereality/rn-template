import {useState} from 'react';
import {AsyncActions} from '../types/actions';

export type ErrorType = {
  message: string;
  statusCode: number;
};

type AsyncActionReturnType<T> = {
  actions: AsyncActions<T>;
  loading: {
    [key: string]: boolean;
  };
  errors: {
    [key: string]: ErrorType | boolean;
  };
};

export const useWithAsyncAction = <T>(
  args: object,
): AsyncActionReturnType<T> => {
  let argsWithLoading = {};
  let argsWithError = {};

  Object.keys(args).forEach((key: string): void => {
    argsWithLoading = {
      ...argsWithLoading,
      [key]: false,
    };
    argsWithError = {
      ...argsWithError,
      [key]: false,
    };
  });

  const [loading, setLoading] = useState(argsWithLoading);
  const [errors, setErrors] = useState(argsWithError);

  const setLoadingState = (key: string, value: boolean): void => {
    setLoading({
      ...argsWithLoading,
      [key]: value,
    });
  };

  const setErrorState = (key: string, value: boolean): void => {
    setErrors({
      ...argsWithError,
      [key]: value,
    });
  };

  let argsWithExecute = {};
  Object.keys(args).forEach((key: string): void => {
    argsWithExecute = {
      ...argsWithExecute,
      [key]: async (data: object | string, param?: string): Promise<T> => {
        try {
          setLoadingState(key, true);
          // @ts-ignore
          const response = await args[key](data, param);
          setLoadingState(key, false);
          setErrorState(key, false);
          return response;
        } catch (error) {
          setLoadingState(key, false);
          setErrorState(key, error.response?.data);
          throw error;
        }
      },
    };
  });

  return {actions: argsWithExecute, loading, errors};
};
