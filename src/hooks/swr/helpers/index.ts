import { Pagination } from 'hooks/swr/types';
// import { axiosOrigin } from 'api/axios-instances';
// import { AxiosResponse } from 'axios';

type sizeSetter = (size: number | ((size: number) => number)) => Promise<any[] | undefined>;

export const paginationData = (
    data: any,
    error: any,
    size: number,
    setSize: sizeSetter,
    pageSize: number,
): Pagination<any> => {
    const isLoadingInitialData = !data && !error;
    const isLoadingMore = isLoadingInitialData || (data && typeof data[size - 1] === 'undefined');
    const isEmpty = typeof data?.[0]?.count === 'number' ? data?.[0].count === 0 : data?.[0]?.length === 0;
    let endData;
    if (data?.[0]?.data) {
        endData = data && data[data?.length - 1]?.data?.length < pageSize;
    } else {
        endData = data && data[data?.length - 1]?.length < pageSize;
    }
    const isReachingEnd = isEmpty || endData;
    const flattedData =
        data
            ?.map((item: any) => {
                if (item.data) {
                    return item.data;
                }
                return item;
            })
            .flat() || [];

    const onEndReached = (): void => {
        if (!isReachingEnd) {
            setSize(size + 1);
        }
    };

    return { flattedData, isLoadingMore, onEndReached, isLoadingInitialData };
};

// export const onboardingFetcher = (token_type: string, access_token: string) => (url: string): Promise<AxiosResponse> =>
//     axiosOrigin
//         .get(url, {
//             headers: {
//                 Authorization: `${token_type} ${access_token}`,
//             },
//         })
//         .then((res: any) => res.data);
