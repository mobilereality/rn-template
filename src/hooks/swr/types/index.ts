import { SWRInfiniteResponseInterface } from 'swr';
import { AxiosError } from 'axios';

export interface Pagination<T> {
    isLoadingMore?: boolean;
    isLoadingInitialData?: boolean;
    onEndReached?: () => void;
    flattedData: Array<T>;
}

export interface PaginationResponse<Data, Item>
    extends SWRInfiniteResponseInterface<Data, AxiosError>,
        Pagination<Item> {}
