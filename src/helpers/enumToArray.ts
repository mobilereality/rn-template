export const enumToArray = (Enum: Record<string, any>): string[] => Object.keys(Enum).map((key) => Enum[key]);
