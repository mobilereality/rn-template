import { Dimensions, PixelRatio } from 'react-native';
import { isIOS } from 'helpers/isIOS';

const { width: SCREEN_WIDTH } = Dimensions.get('window');
const scale = SCREEN_WIDTH / 375;

export const normalize = (px: number): number => {
    const scaledSize = px * scale;

    if (isIOS) {
        return Math.round(PixelRatio.roundToNearestPixel(scaledSize));
    }
    return Math.round(PixelRatio.roundToNearestPixel(scaledSize)) - 2;
};
