import { Alert } from 'react-native';
import { TFunction } from 'i18next';

export const showAlert = (param: string | TFunction, header = ''): void => {
    typeof param === 'string' ? Alert.alert(header, param) : Alert.alert('', param('common.errorAlert'));
};
