import { isIOS } from 'helpers/isIOS';
import KeyboardManager from 'react-native-keyboard-manager';

// prevents issue of keyboard sliding up and cover inputs on IOS
export const handleIOSKeyboard = (): void => {
    if (!isIOS) {
        return;
    }

    return KeyboardManager.reloadLayoutIfNeeded();
};
