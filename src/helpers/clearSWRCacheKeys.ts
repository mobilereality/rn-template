import { cache } from 'swr';

export const clearSWRCacheKeys = (endpoints: string[]): void => {
    const filteredKeys = endpoints.map((item) => cache.keys().filter((key) => key.includes(item))).flat();

    filteredKeys.forEach((key) => cache.delete(key));
};
