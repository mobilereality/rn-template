import { get } from 'lodash';
import { FieldError } from 'react-hook-form';
import { TFunction } from 'i18next';

type FieldErrorWithProps = {
    error: FieldError | undefined;
    props: Record<string, any>;
};

export const resolveErrorMessage = (
    error: FieldError | FieldErrorWithProps | undefined,
    t: TFunction,
): string | undefined => {
    if (get(error, 'message')) {
        return t(get(error, 'message'));
    } else if (get(error, 'error.message')) {
        return t(get(error, 'error.message'), get(error, 'props'));
    }
    return;
};
