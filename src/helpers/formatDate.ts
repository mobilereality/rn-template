export const formatDate = (date: Date | string | undefined | number | null): string | undefined => {
    if (!date) {
        return undefined;
    }

    const newDate = new Date(date);
    const day = newDate.getDate();
    const month = newDate.getMonth() + 1;
    const year = newDate.getFullYear();

    return `${month < 10 ? '0' : ''}${month}/${day < 10 ? '0' : ''}${day}/${year}`;
};
