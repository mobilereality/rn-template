import { isIOS } from 'helpers/isIOS';

jest.mock('react-native/Libraries/Utilities/Platform', () => ({
    OS: 'ios',
}));

describe('Is iOS', () => {
    it('Should return true if iOS os', () => {
        expect(isIOS).toBe(true);
    });
});
