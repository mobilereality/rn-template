import { isIphoneX } from 'helpers/isIPhoneX';

afterEach(() => {
    jest.resetModules();
});

describe('Is iPhone X', () => {
    it('Should return true if iPhone X', () => {
        jest.mock('react-native/Libraries/Utilities/Platform', () => ({
            OS: 'ios',
        }));

        jest.mock('react-native/Libraries/Utilities/Dimensions', () => {
            const Dimensions = jest.requireActual('react-native/Libraries/Utilities/Dimensions');
            Dimensions.get = jest.fn().mockReturnValue({ height: 896, width: 812 });

            return Dimensions;
        });
        expect(isIphoneX()).toBe(true);
    });

    it('Should return false if not iPhone X dimensions', () => {
        jest.mock('react-native/Libraries/Utilities/Platform', () => ({
            OS: 'ios',
        }));

        jest.mock('react-native/Libraries/Utilities/Dimensions', () => {
            const Dimensions = jest.requireActual('react-native/Libraries/Utilities/Dimensions');
            Dimensions.get = jest.fn().mockReturnValue({ height: 1024, width: 768 });

            return Dimensions;
        });
        expect(isIphoneX()).toBe(false);
    });

    it('Should return false if Android device', () => {
        jest.mock('react-native/Libraries/Utilities/Platform', () => ({
            OS: 'android',
        }));

        jest.mock('react-native/Libraries/Utilities/Dimensions', () => {
            const Dimensions = jest.requireActual('react-native/Libraries/Utilities/Dimensions');
            Dimensions.get = jest.fn().mockReturnValue({ height: 896, width: 812 });

            return Dimensions;
        });
        expect(isIphoneX()).toBe(false);
    });
});
