import React, { ReactElement } from 'react';
import { ContainerInfo } from 'containers/container-info';

type State = {
    hasError: boolean;
};

type Props = {
    children: ReactElement;
};

export class ErrorBoundary extends React.Component<Props, State> {
    state = {
        hasError: false,
    };

    static getDerivedStateFromError(): State {
        return { hasError: true };
    }

    render(): ReactElement {
        if (this.state.hasError) {
            return <ContainerInfo textInfoKey={'common.errorAlert'} />;
        }

        return this.props.children;
    }
}
