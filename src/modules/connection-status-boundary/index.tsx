import React, { FC, useEffect, useState } from 'react';
import NetInfo from '@react-native-community/netinfo';
import { ContainerInfo } from 'containers/container-info';

export const ConnectionStatusBoundary: FC = ({ children }) => {
    const [isConnected, setIsConnected] = useState(true);

    useEffect(() => {
        return NetInfo.addEventListener((state) => {
            setIsConnected(!!state.isConnected);
        });
    }, []);

    if (isConnected) {
        return <>{children}</>;
    }

    return <ContainerInfo textInfoKey={'common.noInternet'} />;
};
