import React, { FC, ReactNode } from 'react';
import { ScrollViewProps, StyleSheet, ViewStyle, KeyboardAvoidingView } from 'react-native';
import { StyledScrollView } from 'components/styled-scrollview';

type Props = {
    children: ReactNode;
    containerStyle?: ViewStyle;
} & ScrollViewProps;

export const KeyboardAware: FC<Props> = ({ children, containerStyle, ...props }) => {
    return (
        <StyledScrollView
            keyboardShouldPersistTaps="handled"
            style={styles.container}
            contentContainerStyle={[styles.contentContainer, containerStyle]}
            {...props}>
            <KeyboardAvoidingView style={styles.container}>{children}</KeyboardAvoidingView>
        </StyledScrollView>
    );
};

interface Styles {
    container: ViewStyle;
    contentContainer: ViewStyle;
}

const styles = StyleSheet.create<Styles>({
    container: {
        flex: 1,
    },
    contentContainer: {
        flexGrow: 1,
    },
});
