import React, { FC, useContext } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { RoutesNavigators } from 'navigation/routes';
import { NoAuthNavigator } from 'navigation/no-auth-navigator';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { createStackNavigator } from '@react-navigation/stack';
import { AuthContext } from 'providers/auth-context';
import { MainStackParamList } from 'types/screens';
import { Loader } from 'components/loader';
import { COLORS } from 'styles/colors';

const Stack = createStackNavigator<MainStackParamList>();

export const Authorization: FC = () => {
    const {
        isLoggedIn,
        user,
        loadingInitialData,
    } = useContext(AuthContext);

    const options = {
        animationEnabled: false,
        cardStyle: {
            backgroundColor: COLORS.BLACK,
        },
    };

    return (
        <>
            <SafeAreaProvider style={{ backgroundColor: COLORS.WHITE_30 }}>
                {loadingInitialData ? (
                    <Loader isOpen isFullOpacity />
                ) : (
                    <NavigationContainer>
                        <Stack.Navigator headerMode="none">
                            {isLoggedIn ? (
                                <Stack.Screen
                                    name={RoutesNavigators.AUTH_NAV}
                                    component={null as any}
                                    options={options}
                                    initialParams={{ user }}
                                />
                            ) : (
                                <Stack.Screen
                                    name={RoutesNavigators.NO_AUTH_NAV}
                                    component={NoAuthNavigator}
                                    options={options}
                                />
                            )}
                        </Stack.Navigator>
                    </NavigationContainer>
                )}
            </SafeAreaProvider>
        </>
    );
};
