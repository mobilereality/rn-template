import { StackScreenProps } from '@react-navigation/stack';
import { Routes, RoutesNavigators } from 'navigation/routes';
import { UserResponse } from 'types/auth';

export type MainStackParamList = {
    [RoutesNavigators.AUTH_NAV]: { user: UserResponse };
    [RoutesNavigators.NO_AUTH_NAV]: undefined;
};

export type AuthStackParamList = {};

export type NoAuthStackParamList = {
    [Routes.WELCOME]: undefined;
    [Routes.REGISTER]: undefined;
    [Routes.LOGIN]: undefined;
};

export type AuthNavProps = StackScreenProps<MainStackParamList, RoutesNavigators.AUTH_NAV>;
export type NoAuthNavProps = StackScreenProps<MainStackParamList, RoutesNavigators.NO_AUTH_NAV>;

export type WelcomeScreenProps = StackScreenProps<NoAuthStackParamList, Routes.WELCOME>;
export type RegisterScreenProps = StackScreenProps<NoAuthStackParamList, Routes.REGISTER>;
export type LoginScreenProps = StackScreenProps<NoAuthStackParamList, Routes.LOGIN>;