export type UserResponse = {
    id: string,
    firstName: string,
    lastName: string,
    email: string,
} | null;

export type AuthorizationResponse = {
    id_token: string;
    access_token: string;
    expires_in: number;
    scope: string;
    token_type: string;
    refresh_token?: string;
};