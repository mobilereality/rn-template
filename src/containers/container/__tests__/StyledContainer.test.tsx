import React from 'react';
import { render } from 'react-native-testing-library';
import { StyledContainer } from 'containers/container';

describe('Styled Container', () => {
    it('should generate snapshot', () => {
        const wrapper = render(<StyledContainer />);
        expect(wrapper).toMatchSnapshot();
    });
});
