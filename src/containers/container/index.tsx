import React, { FC } from 'react';
import { SafeAreaView, StatusBar, StyleSheet, View, ViewProps, ViewStyle } from 'react-native';
import { COLORS } from 'styles/colors';
import { SIZES } from 'styles/sizes';

export const StyledContainer: FC<ViewProps> = ({ children, style, ...props }) => {
    return (
        <SafeAreaView style={styles.container} {...props}>
            <StatusBar backgroundColor={COLORS.BLACK} barStyle={'light-content'} />
            <View style={[styles.innerContainer, style]}>{children}</View>
        </SafeAreaView>
    );
};

interface Styles {
    container: ViewStyle;
    innerContainer: ViewStyle;
}

const styles = StyleSheet.create<Styles>({
    container: {
        flex: 1,
        backgroundColor: COLORS.BLACK,
    },
    innerContainer: {
        flex: 1,
        paddingHorizontal: SIZES.DEFAULT_SPACING,
    },
});
