import React, { FC, ReactElement } from 'react';
import { StyleSheet, View, ViewStyle } from 'react-native';
import { Translation } from 'react-i18next';
import { StyledText } from 'components/styled-text';
import { StyledContainer } from 'containers/container';
import { normalize } from 'helpers/normalize';

type Props = {
    textInfoKey: string;
};

export const ContainerInfo: FC<Props> = ({ textInfoKey }) => {
    return (
        <StyledContainer>
            <View style={styles.flex}>
                <Translation>{(t, { }): ReactElement => <StyledText>{t(textInfoKey)}</StyledText>}</Translation>
            </View>
        </StyledContainer>
    );
};

interface Style {
    flex: ViewStyle;
}

interface Style {
    flex: ViewStyle;
    beatClubHeader: ViewStyle;
}

const styles = StyleSheet.create<Style>({
    flex: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    beatClubHeader: {
        alignItems: 'center',
        marginBottom: normalize(30),
    },
});
