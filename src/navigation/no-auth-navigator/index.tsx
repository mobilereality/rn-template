import React, { FC } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { Routes } from 'navigation/routes';
import { WelcomeScreen } from 'screens/onboarding/welcome';
import { NoAuthStackParamList, NoAuthNavProps } from 'types/screens';
import { RegisterScreen } from 'screens/onboarding/register';
import { LoginScreen } from 'screens/onboarding/login';

const NoAuthStack = createStackNavigator<NoAuthStackParamList>();

export const NoAuthNavigator: FC<NoAuthNavProps> = () => {
    return (
        <NoAuthStack.Navigator headerMode="none">
            <NoAuthStack.Screen name={Routes.WELCOME} component={WelcomeScreen} />
            <NoAuthStack.Screen name={Routes.REGISTER} component={RegisterScreen} />
            <NoAuthStack.Screen name={Routes.LOGIN} component={LoginScreen} />
        </NoAuthStack.Navigator>
    );
};
