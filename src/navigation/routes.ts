export enum Routes {
    WELCOME = 'Welcome',
    REGISTER = 'Register',
    LOGIN = 'Login',
}

export enum RoutesNavigators {
    AUTH_NAV = 'Auth nav',
    NO_AUTH_NAV = 'No auth nav',
}
