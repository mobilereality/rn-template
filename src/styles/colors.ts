export const COLORS = {
    WHITE: '#FFFFFF',
    WHITE_30: 'rgba(255, 255, 255, 0.3)',
    WHITE_80: 'rgba(255, 255, 255, 0.8)',
    BLACK: '#000000',
    ERROR_RED: '#E02020',
    TRANSPARENT: 'transparent',
    CYAN: '#38FFFF',
    GREY_75: 'rgba(25, 25, 25, 0.75)',
    GRAY: '#848484',
    TUNDORA: '#434343',
};
