import { normalize } from 'helpers/normalize';

export const SIZES = {
    DEFAULT_SPACING: normalize(25),
    INPUT_HEIGHT: normalize(40),
};
