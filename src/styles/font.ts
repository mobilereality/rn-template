import { normalize } from 'helpers/normalize';

export enum FONT_FAMILIES {
    MAAX_REGULAR = 'Maax',
    MAAX_MEDIUM = 'Maax-Medium',
};

export enum FONT_SIZES {
    BUTTON = normalize(13),
    SMALL = normalize(14),
    REGULAR = normalize(16),
};

export enum FONT_WEIGHTS {
    THIN = '100',
    EXTRA_LIGHT = '200',
    LIGHT = '300',
    REGULAR = '400',
    MEDIUM = '500',
    SEMI_BOLD = '600',
    BOLD = '700',
    EXTRA_BOLD = '800',
    HEAVY = '900',
};