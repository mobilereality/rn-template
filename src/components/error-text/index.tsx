import React, { FC } from 'react';
import { StyleProp, StyleSheet, TextStyle, View, ViewStyle } from 'react-native';
import { normalize } from 'helpers/normalize';
import { COLORS } from 'styles/colors';
import { FONT_SIZES } from 'styles/font';
import { StyledText } from 'components/styled-text';

interface Props {
    children?: string;
    style?: StyleProp<ViewStyle>;
}

export const ErrorText: FC<Props> = ({ children, style }) => {
    if (children) {
        return (
            <View style={[styles.container, style]}>
                {children ? <StyledText style={[styles.text]}>{children}</StyledText> : null}
            </View>
        );
    }

    return null;
};

interface Style {
    container: ViewStyle;
    text: TextStyle;
}

const styles = StyleSheet.create<Style>({
    container: {
        flexDirection: 'row',
        minHeight: normalize(16),
        marginTop: normalize(8),
    },
    text: {
        flexShrink: 1,
        marginTop: 'auto',
        fontSize: FONT_SIZES.REGULAR,
        color: COLORS.ERROR_RED,
    },
});
