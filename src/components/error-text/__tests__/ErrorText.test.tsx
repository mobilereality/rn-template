import React from 'react';
import { render } from 'react-native-testing-library';

import { ErrorText } from 'components/error-text';

describe('Styled Text', () => {
    it('should generate snapshot', () => {
        const wrapper = render(<ErrorText>Test Error</ErrorText>);
        expect(wrapper).toMatchSnapshot();
    });
});
