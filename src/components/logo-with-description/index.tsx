import React, { FC } from 'react';
import { View, ViewStyle, TextStyle, StyleSheet } from 'react-native';
import { Logo } from 'components/logo';
import { StyledText } from 'components/styled-text';
import { normalize } from 'helpers/normalize';
import { useTranslation } from 'react-i18next';

export enum PRESETS {
    DEFAULT,
    LOGIN,
    FULLSCREEN,
}

type Props = {
    description: string;
    stylePreset?: PRESETS;
};

export const LogoWithDescription: FC<Props> = ({
    description,
    stylePreset = PRESETS.DEFAULT
}) => {
    const { text, logoWrapper, defaultPreset, loginPreset, fullScreenPreset } = styles;
    const [t] = useTranslation();

    const resolvePreset = (): ViewStyle => {
        switch (stylePreset) {
            case PRESETS.DEFAULT:
                return defaultPreset
            case PRESETS.LOGIN:
                return loginPreset
            case PRESETS.FULLSCREEN:
                return fullScreenPreset
            default:
                return defaultPreset
        }
    }

    return (
        <View style={[logoWrapper, resolvePreset()]}>
            <Logo />
            <StyledText style={text}>{t(description)}</StyledText>
        </View>
    );
};

type Style = {
    logoWrapper: ViewStyle;
    text: TextStyle;
    defaultPreset: ViewStyle;
    loginPreset: ViewStyle;
    fullScreenPreset: ViewStyle;
};

const styles = StyleSheet.create<Style>({
    logoWrapper: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    text: {
        textAlign: 'center',
        lineHeight: normalize(24),
    },
    defaultPreset: {
        marginTop: normalize(70),
        marginBottom: normalize(60),
    },
    loginPreset: {
        marginTop: normalize(90),
        marginBottom: normalize(84),
    },
    fullScreenPreset: {
        flex: 1,
    }
});