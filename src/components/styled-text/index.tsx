import React, { ReactElement, ReactNode } from 'react';
import { StyleSheet, Text, TextProps, TextStyle } from 'react-native';
import { FONT_FAMILIES, FONT_SIZES } from 'styles/font';

export enum TEXT_VARIANTS {
    REGULAR = 'regular',
    SPACED = 'spaced',
}

interface StyledTextProps extends TextProps {
    children: ReactNode;
    variant?: TEXT_VARIANTS;
}

export const StyledText = (props: StyledTextProps): ReactElement => {
    const { children, style, variant } = props;
    const { regular, spaced } = styles;

    const resolveVariant = (): TextStyle => {
        switch (variant) {
            case TEXT_VARIANTS.REGULAR: {
                return regular;
            }
            case TEXT_VARIANTS.SPACED: {
                return spaced;
            }
            default: {
                return regular;
            }
        }
    };

    return (
        <Text {...props} style={[resolveVariant(), style]}>
            {children}
        </Text>
    );
};

interface Style {
    regular: TextStyle;
    spaced: TextStyle;
}

const styles = StyleSheet.create<Style>({
    regular: {
        fontSize: FONT_SIZES.REGULAR,
        fontFamily: FONT_FAMILIES.MAAX_REGULAR,
    },
    spaced: {
        fontSize: FONT_SIZES.SMALL,
        textTransform: 'uppercase',
        fontFamily: FONT_FAMILIES.MAAX_MEDIUM,
        letterSpacing: 1,
    },
});
