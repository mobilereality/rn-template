import React from 'react';
import { render } from 'react-native-testing-library';

import { StyledText } from 'components/styled-text';

describe('Styled Text', () => {
    it('should generate snapshot', () => {
        const wrapper = render(<StyledText>Lorem text</StyledText>);
        expect(wrapper).toMatchSnapshot();
    });
});
