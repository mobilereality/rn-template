import React, { FC } from 'react';
import { Text, TextStyle, StyleSheet } from 'react-native';
import { normalize } from 'helpers/normalize';
import { SIZES } from 'styles/sizes';

export const Logo: FC = () => {
    const { logo } = styles;

    return <Text style={logo}>Logo</Text>;
};

type Style = {
    logo: TextStyle;
};

const styles = StyleSheet.create<Style>({
    logo: {
        width: normalize(75),
        height: normalize(77),
        borderWidth: 1,
        textAlign: 'center',
        marginBottom: SIZES.DEFAULT_SPACING,
    },
});

