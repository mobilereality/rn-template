import React, { FC, useState } from 'react';
import { get } from 'lodash';
import { StyleSheet, TextInput, TextStyle, View, ViewStyle } from 'react-native';
import { Control, FieldError, useController } from 'react-hook-form';
import { StyledText, TEXT_VARIANTS } from 'components/styled-text';
import { COLORS } from 'styles/colors';
import { resolveErrorMessage } from 'helpers/resolveErrorMessage';
import { useTranslation } from 'react-i18next';
import { ErrorText } from 'components/error-text';
import { TextInputMask } from 'react-native-masked-text';
import { FONT_FAMILIES } from 'styles/font';

type Props = {
    name: string;
    label: string;
    control: Control<any>;
    error?: FieldError;
    containerStyle?: ViewStyle;
    mask?: string;
    onTouchStart?: () => void;
    defaultValue?: string;
    editable?: boolean;
    disabled?: boolean;
    numberOfLines?: number;
    placeholder?: string;
};

export const StyledInput: FC<Props> = ({
    name,
    defaultValue,
    label,
    control,
    error,
    mask,
    containerStyle,
    onTouchStart,
    editable,
    placeholder,
    disabled,
    numberOfLines,
}) => {
    const [isFocused, setIsFocused] = useState(false);
    const [t] = useTranslation();
    const { input, inputError, text, inputFocus } = styles;
    const { field } = useController({
        name,
        control,
        defaultValue,
    });
    const { value, onChange } = field;
    const errorMessage = resolveErrorMessage(error, t);
    const isError = get(error, 'message') || get(error, 'error.message');

    const commonInputProps = {
        value,
        defaultValue,
        editable,
        style: [input, isError && inputError, isFocused && !isError && inputFocus],
        onChangeText: onChange,
        placeholder: placeholder || t('common.pleaseEnter', { label }),
        onFocus: (): void => setIsFocused(true),
        onBlur: (): void => setIsFocused(false),
        placeholderTextColor: disabled || isError ? COLORS.GRAY : COLORS.BLACK,
    };

    const defaultInput = (
        <TextInput
            onTouchStart={onTouchStart}
            multiline={!!numberOfLines}
            numberOfLines={numberOfLines}
            {...commonInputProps}
        />
    );

    const maskedInput = (
        <TextInputMask type={'custom'} options={{ mask }} includeRawValueInChangeText={false} {...commonInputProps} />
    );

    return (
        <View style={containerStyle} pointerEvents={disabled ? 'none' : 'auto'}>
            <StyledText variant={TEXT_VARIANTS.SPACED} style={text}>
                {label}
            </StyledText>
            {mask ? maskedInput : defaultInput}
            <ErrorText>{errorMessage}</ErrorText>
        </View>
    );
};

type Style = {
    input: ViewStyle;
    inputFocus: ViewStyle;
    inputError: ViewStyle;
    text: TextStyle;
};

const styles = StyleSheet.create<Style>({
    input: {
        width: '100%',
        borderBottomWidth: 1,
        borderColor: COLORS.GRAY,
        textAlignVertical: 'top',
    },
    inputFocus: {
        borderColor: COLORS.TUNDORA,
    },
    inputError: {
        borderColor: COLORS.ERROR_RED,
    },
    text: {
        color: COLORS.GRAY,
        fontFamily: FONT_FAMILIES.MAAX_REGULAR,
    },
});
