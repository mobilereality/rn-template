import React, { FC } from 'react';
import { StyleSheet, TextStyle, TouchableOpacity, ViewStyle } from 'react-native';
import { normalize } from 'helpers/normalize';
import { COLORS } from 'styles/colors';
import { FONT_SIZES } from 'styles/font';
import { StyledText, TEXT_VARIANTS } from 'components/styled-text';

type Props = {
    title: string;
    onPress: () => void;
    containerStyle?: ViewStyle;
};

export const StyledButton: FC<Props> = ({ title, onPress, containerStyle }) => {
    const { container, text } = styles;
    return (
        <TouchableOpacity style={[container, containerStyle]} onPress={onPress}>
            <StyledText style={text} variant={TEXT_VARIANTS.SPACED}>
                {title}
            </StyledText>
        </TouchableOpacity>
    );
};

type Style = {
    container: ViewStyle;
    text: TextStyle;
};

const styles = StyleSheet.create<Style>({
    container: {
        width: '100%',
        height: normalize(65),
        backgroundColor: COLORS.TUNDORA,
        alignItems: 'center',
        justifyContent: 'center',
        position: 'relative',
    },
    text: {
        color: COLORS.WHITE,
        fontSize: FONT_SIZES.BUTTON,
    },
});
