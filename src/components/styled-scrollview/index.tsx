import React, { FC, ReactNode } from 'react';
import { ScrollView, ScrollViewProps } from 'react-native';

interface StyledScrollViewProps extends ScrollViewProps {
    children: ReactNode;
}

export const StyledScrollView: FC<StyledScrollViewProps> = (props) => {
    return (
        <ScrollView
            {...props}
            showsHorizontalScrollIndicator={false}
            showsVerticalScrollIndicator={false}
            bounces={false}
        />
    );
};
