import React, { FC } from 'react';
import { StyleSheet, TouchableOpacity, View, ViewStyle } from 'react-native';
import { normalize } from 'helpers/normalize';
import { COLORS } from 'styles/colors';
import { StyledText, TEXT_VARIANTS } from 'components/styled-text';

type Props = {
    title: string;
    onPress: () => void;
    containerStyle?: ViewStyle;
};

export const ArrowButton: FC<Props> = ({ title, onPress, containerStyle }) => {
    const { container, arrow } = styles;
    return (
        <TouchableOpacity style={[container, containerStyle]} onPress={onPress}>
            <StyledText variant={TEXT_VARIANTS.SPACED}>{title}</StyledText>
            <View style={arrow} />
        </TouchableOpacity>
    );
};

type Style = {
    container: ViewStyle;
    arrow: ViewStyle;
};

const styles = StyleSheet.create<Style>({
    container: {
        width: '100%',
        height: normalize(65),
        backgroundColor: COLORS.WHITE_80,
        alignItems: 'center',
        justifyContent: 'center',
        position: 'relative',
    },
    arrow: {
        position: 'absolute',
        right: normalize(25),
        width: normalize(15),
        height: normalize(15),
        transform: [{ rotate: '45deg' }],
        borderRightWidth: 1,
        borderTopWidth: 1,
        borderColor: COLORS.BLACK,
    },
});
