import React, { ReactElement } from 'react';
import { FlatList, FlatListProps } from 'react-native';

export const StyledFlatList = (props: FlatListProps<any>): ReactElement => {
    return (
        <FlatList
            {...props}
            bounces={false}
            showsVerticalScrollIndicator={false}
            showsHorizontalScrollIndicator={false}
        />
    );
};
