import React, { FC, memo, useEffect, useRef } from 'react';
import { ActivityIndicator, Animated, StyleSheet, ViewStyle } from 'react-native';
import { COLORS } from 'styles/colors';
import { SELECT_INDEX } from 'styles/zIndex';
import isEqual from 'lodash.isequal';

interface Props {
    isOpen: boolean;
    isFullOpacity?: boolean;
    noBackground?: boolean;
}

export const Loader: FC<Props> = memo(
    ({ isOpen, isFullOpacity, noBackground }) => {
        const loaderAnimation = useRef(new Animated.Value(0)).current;

        useEffect(() => {
            Animated.timing(loaderAnimation, {
                toValue: isOpen ? 1 : 0,
                duration: 300,
                useNativeDriver: true,
            }).start();
        }, [isOpen]);

        const animationStyles = {
            opacity: loaderAnimation.interpolate({
                inputRange: [0, 1],
                outputRange: [0, 1],
            }),
        };

        return (
            <Animated.View
                style={[
                    styles.container,
                    animationStyles,
                    isFullOpacity && { backgroundColor: COLORS.BLACK },
                    noBackground && { backgroundColor: COLORS.TRANSPARENT },
                ]}
                pointerEvents={isOpen ? 'auto' : 'none'}>
                <ActivityIndicator size={'large'} color={isFullOpacity ? COLORS.CYAN : COLORS.WHITE} />
            </Animated.View>
        );
    }, isEqual);

interface Styles {
    container: ViewStyle;
}

const styles = StyleSheet.create<Styles>({
    container: {
        ...StyleSheet.absoluteFillObject,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: COLORS.WHITE_30,
        zIndex: SELECT_INDEX,
    },
});
