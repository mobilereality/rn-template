import React from 'react';
import { render } from 'react-native-testing-library';
import { Loader } from 'components/loader';

describe('Loader', () => {
    it('should generate snapshot', () => {
        const wrapper = render(<Loader isOpen={true} />);
        expect(wrapper).toMatchSnapshot();
    });
});
