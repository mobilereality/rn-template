import React, { ReactElement, useState } from 'react';
import { StyleSheet, ViewStyle, View, TouchableOpacity } from 'react-native';
import DateTimePicker, { Event } from '@react-native-community/datetimepicker';
import { Control, FieldError, SetFieldValue, useWatch } from 'react-hook-form';
import { StyledInput } from 'components/input';
import { StyledButton } from 'components/styled-button';
import { useTranslation } from 'react-i18next';
import { formatDate } from 'helpers/formatDate';
import { COLORS } from 'styles/colors';
import Modal from 'react-native-modal';
import { isIOS } from 'helpers/isIOS';

type Props = {
    control: Control<any>;
    name: string;
    label: string;
    setValue: SetFieldValue<any>;
    maximumDate?: Date;
    minimumDate?: Date;
    clickableInput?: boolean;
    error?: FieldError;
    defaultValue?: Date | null;
    trigger?: any;
    containerStyle?: ViewStyle;
};

export const StyledDatePicker = ({
    control,
    name,
    label,
    setValue,
    maximumDate,
    minimumDate,
    error,
    defaultValue,
    trigger,
    containerStyle,
}: Props): ReactElement => {
    const { t } = useTranslation();
    const [isDatePickerVisible, setDatePickerVisibility] = useState(false);

    const onChange = (event: Event, selectedDate?: Date): void => {
        if (!isIOS) {
            setDatePickerVisibility(false);
        }

        const currentDate = selectedDate || defaultValue;
        setValue(name, currentDate, { shouldDirty: true, shouldValidate: true });

        if (trigger) {
            trigger(name);
        }
    };

    const time = useWatch({
        control,
        name,
        defaultValue,
    });

    const androidInput = (
        <TouchableOpacity style={containerStyle} onPress={(): void => setDatePickerVisibility(true)}>
            <StyledInput
                label={label}
                control={control}
                defaultValue={time ? formatDate(time) : undefined}
                error={error}
                name={name}
                editable={false}
            />
        </TouchableOpacity>
    );

    const iosInput = (
        <StyledInput
            label={label}
            control={control}
            defaultValue={time ? formatDate(time) : undefined}
            error={error}
            name={name}
            containerStyle={containerStyle}
            editable={false}
            onTouchStart={(): void => setDatePickerVisibility(true)}
        />
    );

    const androidPicker = (): ReactElement | null => {
        if (!isDatePickerVisible) {
            return null;
        }

        return (
            <DateTimePicker
                value={time || new Date()}
                mode="date"
                display="default"
                onChange={(event: Event, selectedDate: Date | undefined): void => {
                    if (event.type === 'set') {
                        onChange(event, selectedDate);
                    } else {
                        setDatePickerVisibility(false);
                    }
                }}
                maximumDate={maximumDate}
                minimumDate={minimumDate}
            />
        );
    };

    const iosPicker = (
        <Modal
            coverScreen={true}
            isVisible={isDatePickerVisible}
            backdropTransitionOutTiming={0}
            animationIn="fadeIn"
            animationOut="fadeOut">
            <View style={styles.modalContainer}>
                <DateTimePicker
                    value={time || new Date()}
                    display="spinner"
                    onChange={onChange}
                    maximumDate={maximumDate}
                    minimumDate={minimumDate}
                    textColor={COLORS.WHITE}
                    style={styles.picker}
                />
                <StyledButton
                    title={t('common.ok')}
                    onPress={(): void => {
                        if (!time) {
                            setValue(name, new Date(), { shouldDirty: true, shouldValidate: true });
                        }
                        setDatePickerVisibility(false);
                    }}
                    containerStyle={styles.button}
                />
            </View>
        </Modal>
    );

    return (
        <>
            {!isIOS ? androidInput : iosInput}
            {!isIOS ? androidPicker() : iosPicker}
        </>
    );
};

type Styles = {
    button: ViewStyle;
    modalContainer: ViewStyle;
    picker: ViewStyle;
};

const styles = StyleSheet.create<Styles>({
    modalContainer: {
        justifyContent: 'center',
        height: '100%',
        backgroundColor: COLORS.TUNDORA,
    },
    button: {
        marginTop: 25,
        width: '50%',
        alignSelf: 'center',
    },
    picker: {
        backgroundColor: COLORS.BLACK,
    },
});
