import React, { useEffect, useState, FC, Dispatch, SetStateAction } from 'react';
import { useAsyncStorage } from '@react-native-async-storage/async-storage';
import { StorageKeys } from 'constants/storage-keys';
import { clearSWRCacheKeys } from 'helpers/clearSWRCacheKeys';
import { UserResponse, AuthorizationResponse } from 'types/auth';
import RNBootSplash from 'react-native-bootsplash';
// import Api from 'api/api';
// import { ENDPOINTS } from 'api/endpoints';

export interface AuthContext {
    isLoggedIn: boolean;
    setIsLoggedIn: (isLogged: boolean) => void;
    token: AuthorizationResponse;
    setToken: Dispatch<SetStateAction<AuthorizationResponse>>;
    handleToken: (token: any | null) => Promise<void>;
    loadingInitialData: boolean;
    setUser: (user: UserResponse) => void;
    user: UserResponse;
};

const defaultToken: AuthorizationResponse = {
    id_token: '',
    access_token: '',
    expires_in: 0,
    scope: '',
    token_type: '',
    refresh_token: '',
};

export const AuthContext = React.createContext<AuthContext>({
    isLoggedIn: false,
    setIsLoggedIn: () => { },
    token: defaultToken,
    setToken: () => { },
    handleToken: async () => { },
    loadingInitialData: false,
    setUser: () => { },
    user: null,
});

export const AuthContextProvider: FC = ({ children }) => {
    const [isLoggedIn, setIsLoggedIn] = useState<boolean>(false);
    const [loadingInitialData, setLoadingInitialData] = useState<boolean>(false);
    const [user, setUser] = useState<UserResponse>(null);
    const [token, setToken] = useState<AuthorizationResponse>({
        id_token: '',
        access_token: '',
        expires_in: 0,
        scope: '',
        token_type: '',
        refresh_token: '',
    });

    const { setItem, getItem, removeItem } = useAsyncStorage(StorageKeys.TOKEN_DATA);

    const clearAppUserData = async (): Promise<void> => {
        try {
            setUser(null);
            clearSWRCacheKeys([]);
            await removeItem();
            setIsLoggedIn(false);
        } catch (e) { }
    };

    const handleToken = async (token: any | null): Promise<void> => {
        try {
            setLoadingInitialData(true);
            const stringData = token && JSON.stringify(token);
            if (stringData) {
                await setItem(stringData);
                setIsLoggedIn(true);
                return;
            }
            await clearAppUserData();
        } catch (e) {
        } finally {
            setLoadingInitialData(false);
        }
    };

    useEffect(() => {
        (async function (): Promise<void> {
            try {
                const data = await getItem();
                const token: any | null = data ? JSON.parse(data) : null;
                if (token) {
                    // const { data: userData } = await Api.getMe({
                    //     access_token: token.access_token,
                    //     token_type: token.token_type,
                    // });
                    // setUser(userData);
                    await handleToken(token);
                }
            } catch (e) {
                await handleToken(null);
            } finally {
                RNBootSplash.hide({
                    fade: true,
                });
            }
        })();
    }, []);

    return (
        <AuthContext.Provider
            value={{
                isLoggedIn,
                setIsLoggedIn,
                handleToken,
                token,
                setToken,
                user,
                setUser,
                loadingInitialData,
            }}>
            {children}
        </AuthContext.Provider>
    );
};
