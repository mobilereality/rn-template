import React, { FC } from 'react';
import MarketPlaceCardBackground from 'assets/images/backgrounds/marketplace_card.jpg';
import { View, ImageBackground, ViewStyle, StyleSheet } from 'react-native';
import { normalize } from 'helpers/normalize';
import { ArrowButton } from 'components/arrow-button';
import { LogoWithDescription, PRESETS } from 'components/logo-with-description';
import { WelcomeScreenProps } from 'types/screens';
import { useNavigation } from '@react-navigation/native';
import { Routes } from 'navigation/routes';

export const WelcomeScreen: FC<WelcomeScreenProps> = () => {
    const { fullScreen, buttonsWrapper, button } = styles;
    const { navigate } = useNavigation();

    return (
        <ImageBackground source={MarketPlaceCardBackground} style={fullScreen}>
            <LogoWithDescription description="welcome.welcome" stylePreset={PRESETS.FULLSCREEN} />
            <View style={buttonsWrapper}>
                <ArrowButton title="register" onPress={(): void => navigate(Routes.REGISTER)} containerStyle={button} />
                <ArrowButton title="login" onPress={(): void => navigate(Routes.LOGIN)} />
            </View>
        </ImageBackground>
    );
};

type Style = {
    fullScreen: ViewStyle;
    button: ViewStyle;
    buttonsWrapper: ViewStyle;
};

const styles = StyleSheet.create<Style>({
    fullScreen: {
        flex: 1,
    },
    button: {
        marginBottom: normalize(3),
    },
    buttonsWrapper: {
        marginBottom: normalize(65),
    },
});
