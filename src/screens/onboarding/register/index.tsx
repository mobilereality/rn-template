import React, { FC } from 'react';
import { StyleSheet, TextStyle, View, ViewStyle } from 'react-native';
import { LogoWithDescription } from 'components/logo-with-description';
import { StyledText, TEXT_VARIANTS } from 'components/styled-text';
import { normalize } from 'helpers/normalize';
import { useTranslation } from 'react-i18next';
import { RegisterForm } from 'screens/onboarding/register/components/register-form';
import { StyledScrollView } from 'components/styled-scrollview';
import { RegisterScreenProps } from 'types/screens';

export const RegisterScreen: FC<RegisterScreenProps> = () => {
    const { container } = styles;
    const [t] = useTranslation();

    return (
        <StyledScrollView>
            <View style={container}>
                <StyledText variant={TEXT_VARIANTS.SPACED}>
                    {t('register.register')}
                </StyledText>
                <LogoWithDescription description="register.pleaseEnter" />
                <RegisterForm />
            </View>
        </StyledScrollView>
    );
};

type Style = {
    container: ViewStyle;
};

const styles = StyleSheet.create<Style>({
    container: {
        flex: 1,
        alignItems: 'center',
        padding: normalize(25),
    },
});
