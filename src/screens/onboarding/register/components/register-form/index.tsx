import React, { FC } from 'react';
import { View, ViewStyle, StyleSheet, TextStyle } from 'react-native';
import { useForm } from 'react-hook-form';
import { StyledInput } from 'components/input';
import { StyledButton } from 'components/styled-button';
import { useTranslation } from 'react-i18next';
import { normalize } from 'helpers/normalize';
import { StyledText } from 'components/styled-text';
import { validationSchema, Form, FORM_FIELDS } from 'screens/onboarding/register/validators/validationSchema';
import { TEXT_VARIANTS } from 'components/styled-text';
import { StyledDatePicker } from 'components/styled-date-picker';
import { MASKS } from 'constants/masks';
import { SIZES } from 'styles/sizes';

export const RegisterForm: FC = () => {
    const [t] = useTranslation();
    const { container, text, margin } = styles;

    const { control, handleSubmit, formState, setValue } = useForm<Form>({
        resolver: validationSchema,
        defaultValues: {
            [FORM_FIELDS.MEMBER_ID]: '',
            [FORM_FIELDS.FIRST_NAME]: '',
            [FORM_FIELDS.LAST_NAME]: '',
            [FORM_FIELDS.DATE_OF_BIRTH]: '',
            [FORM_FIELDS.SOCIAL_SECURITY_NUMBER]: '',
        },
    });

    const onSubmit = (data: any): void => {
        // eslint-disable-next-line no-console
        console.log('data', data);
    };

    return (
        <View style={container}>
            <View style={margin}>
                <StyledInput
                    label={t('register.memberId')}
                    name={FORM_FIELDS.MEMBER_ID}
                    control={control}
                    error={formState.errors.memberId}
                />
                <StyledText variant={TEXT_VARIANTS.SPACED} style={text}>
                    {t('common.or')}
                </StyledText>
                <StyledInput
                    label={t('common.firstName')}
                    name={FORM_FIELDS.FIRST_NAME}
                    control={control}
                    error={formState.errors.firstName}
                    containerStyle={margin}
                />
                <StyledInput
                    label={t('common.lastName')}
                    name={FORM_FIELDS.LAST_NAME}
                    control={control}
                    error={formState.errors.lastName}
                    containerStyle={margin}
                />
                <StyledDatePicker
                    label={t('register.dateOfBirth')}
                    name={FORM_FIELDS.DATE_OF_BIRTH}
                    control={control}
                    setValue={setValue}
                    maximumDate={new Date()}
                    containerStyle={margin}
                    error={formState.errors.dateOfBirth}
                />
                <StyledInput
                    label={t('register.socialSecurityNumber')}
                    name={FORM_FIELDS.SOCIAL_SECURITY_NUMBER}
                    control={control}
                    error={formState.errors.socialSecurityNumber}
                    mask={MASKS.SOCIAL_SECURITY_NUMBER}
                />
            </View>
            <StyledButton title={t('common.verify')} onPress={handleSubmit(onSubmit)} />
        </View>
    );
};

type Style = {
    container: ViewStyle;
    text: TextStyle;
    margin: TextStyle;
};

const styles = StyleSheet.create<Style>({
    container: {
        flex: 1,
        width: '100%',
    },
    text: {
        padding: normalize(50),
        textDecorationLine: 'underline',
        alignSelf: 'center',
    },
    margin: {
        marginBottom: SIZES.DEFAULT_SPACING,
    },
});
