import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';

export enum FORM_FIELDS {
    MEMBER_ID = 'memberId',
    FIRST_NAME = 'firstName',
    LAST_NAME = 'lastName',
    DATE_OF_BIRTH = 'dateOfBirth',
    SOCIAL_SECURITY_NUMBER = 'socialSecurityNumber',
}

export type Form = {
    [FORM_FIELDS.MEMBER_ID]: string;
    [FORM_FIELDS.FIRST_NAME]: string;
    [FORM_FIELDS.LAST_NAME]: string;
    [FORM_FIELDS.DATE_OF_BIRTH]: string;
    [FORM_FIELDS.SOCIAL_SECURITY_NUMBER]: string;
};

export const validationSchema = yupResolver(
    yup.object().shape({
        [FORM_FIELDS.MEMBER_ID]: yup.string().required('errors.fieldIsRequired'),
        [FORM_FIELDS.FIRST_NAME]: yup.string().required('errors.fieldIsRequired'),
        [FORM_FIELDS.LAST_NAME]: yup.string().required('errors.fieldIsRequired'),
        [FORM_FIELDS.DATE_OF_BIRTH]: yup.string().required('errors.fieldIsRequired'),
        [FORM_FIELDS.SOCIAL_SECURITY_NUMBER]: yup.string().required('errors.fieldIsRequired'),
    }),
);
