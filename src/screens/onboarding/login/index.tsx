import React, { FC } from 'react';
import { StyleSheet, View, ViewStyle } from 'react-native';
import { LogoWithDescription, PRESETS } from 'components/logo-with-description';
import { StyledText, TEXT_VARIANTS } from 'components/styled-text';
import { normalize } from 'helpers/normalize';
import { useTranslation } from 'react-i18next';
import { LoginForm } from 'screens/onboarding/login/components/login-form';
import { StyledScrollView } from 'components/styled-scrollview';
import { LoginScreenProps } from 'types/screens';

export const LoginScreen: FC<LoginScreenProps> = () => {
    const { container } = styles;
    const [t] = useTranslation();

    return (
        <StyledScrollView>
            <View style={container}>
                <StyledText variant={TEXT_VARIANTS.SPACED}>
                    {t('login.login')}
                </StyledText>
                <LogoWithDescription description="login.welcomeBack" stylePreset={PRESETS.LOGIN} />
                <LoginForm />
            </View>
        </StyledScrollView>
    );
};

type Style = {
    container: ViewStyle;
};

const styles = StyleSheet.create<Style>({
    container: {
        flex: 1,
        alignItems: 'center',
        padding: normalize(25),
    },
});
