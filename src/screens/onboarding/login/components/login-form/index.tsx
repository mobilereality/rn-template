import React, { FC } from 'react';
import { View, ViewStyle, StyleSheet, TextStyle, TouchableOpacity } from 'react-native';
import { useForm } from 'react-hook-form';
import { StyledText } from 'components/styled-text';
import { StyledInput } from 'components/input';
import { StyledButton } from 'components/styled-button';
import { useTranslation } from 'react-i18next';
import { normalize } from 'helpers/normalize';
import { validationSchema, Form, FORM_FIELDS } from 'screens/onboarding/login/validators/validationSchema';
import { SIZES } from 'styles/sizes';
import { FONT_SIZES, FONT_WEIGHTS } from 'styles/font';
import { COLORS } from 'styles/colors';

export const LoginForm: FC = () => {
    const [t] = useTranslation();
    const { container, margin, smallMargin, forgotPasswordLabel } = styles;

    const { control, handleSubmit, formState, setValue } = useForm<Form>({
        resolver: validationSchema,
        defaultValues: {
            [FORM_FIELDS.EMAIL]: '',
            [FORM_FIELDS.PASSWORD]: '',
        },
    });

    const onSubmit = (data: any): void => {
        // eslint-disable-next-line no-console
        console.log('data', data);
    };

    return (
        <View style={container}>
            <View style={margin}>
                <StyledInput
                    label={t('common.email')}
                    name={FORM_FIELDS.EMAIL}
                    control={control}
                    error={formState.errors.email}
                    containerStyle={margin}
                />
                <StyledInput
                    label={t('common.password')}
                    name={FORM_FIELDS.PASSWORD}
                    control={control}
                    error={formState.errors.password}
                    containerStyle={smallMargin}
                />
                {/* TODO - ADD REDIRECT */}
                <TouchableOpacity onPress={() => { }}>
                    <StyledText style={forgotPasswordLabel}>Forgot Password</StyledText>
                </TouchableOpacity>
            </View>
            <StyledButton title={t('common.submit')} onPress={handleSubmit(onSubmit)} />
        </View>
    );
};

type Style = {
    container: ViewStyle;
    smallMargin: TextStyle;
    margin: TextStyle;
    forgotPasswordLabel: TextStyle;
};

const styles = StyleSheet.create<Style>({
    container: {
        flex: 1,
        width: '100%',
    },
    smallMargin: {
        marginBottom: normalize(5),
    },
    margin: {
        marginBottom: SIZES.DEFAULT_SPACING,
    },
    forgotPasswordLabel: {
        fontSize: FONT_SIZES.SMALL,
        fontWeight: FONT_WEIGHTS.MEDIUM,
        color: COLORS.GREY_75
    }
});
