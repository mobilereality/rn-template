import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';

export enum FORM_FIELDS {
    EMAIL = 'email',
    PASSWORD = 'password',
};

export type Form = {
    [FORM_FIELDS.EMAIL]: string,
    [FORM_FIELDS.PASSWORD]: string,
}

export const validationSchema = yupResolver(
    yup.object().shape({
        [FORM_FIELDS.EMAIL]: yup.string().required('errors.fieldIsRequired'),
        [FORM_FIELDS.PASSWORD]: yup.string().required('errors.fieldIsRequired'),
    }),
);
