import Config from 'react-native-config';

const ENV = {
    ENVIRONMENT: Config.ENVIRONMENT,
    VERSION: Config.VERSION,
    API_URL: Config.API_URL,
    SENTRY: Config.SENTRY,
};

export default ENV;
