import Axios from 'axios';
import ENV from 'config/env';

export const axiosOrigin = Axios.create({
    baseURL: ENV.API_URL,
});

axiosOrigin.interceptors.response.use(
    (res) => res,
    (error) => {
        throw error.response;
    },
);