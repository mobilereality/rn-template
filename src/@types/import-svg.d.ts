declare module '*.svg' {
    import { SvgProps } from 'react-native-svg';
    const svg: React.FC<SvgProps>;
    export default svg;
}
