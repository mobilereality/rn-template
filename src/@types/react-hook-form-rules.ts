import * as React from 'react';
import { ValidationRule } from 'react-hook-form/dist/types';

export type rules = Partial<{
    required:
    | string
    | boolean
    | React.ReactElement<
        any,
        | string
        | ((
            props: any,
        ) => React.ReactElement<
            any,
            string | any | (new (props: any) => React.Component<any, any, any>)
        > | null)
        | (new (props: any) => React.Component<any, any, any>)
    >
    | ValidationRule;
    min: ValidationRule;
    max: ValidationRule;
    maxLength: ValidationRule;
    minLength: ValidationRule;
    pattern: ValidationRule;
    validate: ValidationRule;
}>;
